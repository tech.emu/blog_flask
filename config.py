import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'yoU-will-never-guess2019*$12'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['ublog.tech@gmail.com']

    POSTS_PER_PAGE = os.environ.get('POSTS_PER_PAGE') or 6

    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
    # set ELASTICSEARCH_URL=http://localhost:9200

    OAUTH_CREDENTIALS = {'google': {'id': '545367371354-9oas1l8jkqn97v46trqjsifni0a218n3.apps.googleusercontent.com',
                                    'secret': 'YXQoT3rYUhFnKF1vjB9sY6ix'}}

    USE_SESSION_FOR_NEXT = True
